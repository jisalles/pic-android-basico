package br.com.picandroid;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Tela2 extends Activity {

	protected EditText nome;
	protected Button exibirNome;
	protected TextView label;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tela2);
		
		nome = (EditText) findViewById(R.id.textoNome);
		exibirNome = (Button) findViewById(R.id.btnExibirMsg);
		label = (TextView) findViewById(R.id.label);
		
		Bundle bundle = getIntent().getExtras();
		
		if (bundle != null) {
			label.setText(bundle.getString("labelNome"));
		}


		// evento click do bot�o
		exibirNome.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				// Toast exibe uma msg rapida na tela
				Toast.makeText(Tela2.this, "Ol� : " + nome.getText(),
						Toast.LENGTH_LONG).show();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.tela2, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
